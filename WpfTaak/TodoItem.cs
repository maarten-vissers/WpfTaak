﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTaak
{
    class TodoItem : INotifyPropertyChanged
    {
        private string beschrijving;
        private bool done;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Beschrijving
        {
            get
            {
                return beschrijving;
            }
            set
            {
                beschrijving = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Beschrijving"));
                }
            }
        }
        public bool Done
        {
            get
            {
                return done;
            }
            set
            {
                done = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Done"));
                }
            }
        }
    }
}
